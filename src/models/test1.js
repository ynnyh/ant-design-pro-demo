import { fakerTest } from '../services/api';

export default {
  namespace: 'test1',

  effects: {
    *testSubmit({ payload }, { call }) {
      console.log('test1');
      const response = yield call(fakerTest, payload);
      console.log(response);
    },
  },

};
