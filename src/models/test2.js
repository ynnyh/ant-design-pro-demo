import { fakerData } from '../services/api';

export default {
  namespace: 'test2',

  effects: {
    *testSubmit({ payload }, { call }) {
      console.log('test2');
      const response = yield call(fakerData, payload);
      console.log(response);
    },
  },

};
