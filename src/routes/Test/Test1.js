import React, { Component } from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';

@connect()
export default class Test1 extends Component {

  handleClick = () => {
    console.log(1);
    this.props.dispatch({
      type: 'test1/testSubmit'
    })
  };

  render() {
    return (
      <button onClick={this.handleClick}>点击测试</button>
    );
  }
}
